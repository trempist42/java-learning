public class Demo {
    public static void main(String[] args) {
        int value = 123456789, sumOfDigits = 0;

        while (value >= 10) {
            sumOfDigits += value % 10;
            value /= 10;
        }
        sumOfDigits += value;
        System.out.println("Sum of digits is "+sumOfDigits);
    }
}
