public class Hello {
    public static void main(String[] args) {
        System.out.println("Area of a rectangle: "+area(14.25d, 10.65d));
        System.out.println("Area of a square: "+area(6.0d));
    }
    public static double area(double length, double width) {
        return length * width;
    }
    public static double area(double side) {
        return side * side;
    }
}
