public class Hello {
    public static void main(String[] args) {
        boolean x = true;
        boolean y = false;

        if (x && y) {
            System.out.println("Condition is true");
        } else {
            System.out.println("Condition is false");
        }
    }
}
