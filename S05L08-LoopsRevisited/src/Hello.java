public class Hello {
    public static void main(String[] args) {
/*
        for (int i = 1, j = 1; i < 10 || j < 10 ; i++, j++) {
            System.out.println("i="+i+", j="+j);
        }
*/
        int i = 1, j = 1;
        while (i < 10 || j < 10) {
            System.out.println("i="+i+", j="+j);
            i++;
            j++;
        }
    }
}
