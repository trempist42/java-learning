public class Hello {
    public static void main(String[] args) {
/*
        for (int i = 1000; i >= 1; i--) {
            System.out.println("The value of i is " + i + ".");
        }
        for (;;) {
            System.out.println("This is infinite loop");
        }
*/
        int isPrime = 13;
        int i = 2;
        for (; i < isPrime; i++) {
            if (isPrime % i == 0) {
                System.out.println("The number is not prime!");
                break;
            }
        }
        if (i == isPrime && isPrime > 1) {
            System.out.println("The number is prime!");
        } else if (isPrime < 2) {
            System.out.println("The number is not prime!");
        }
    }
}
