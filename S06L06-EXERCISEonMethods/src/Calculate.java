public class Calculate {
    public static boolean isPrime(int num) {
        if (num < 2) {
            return false;
        }
        for (int i = 2; i < num; i++) {
            if ((num % i) == 0) {
                return false;
            }
        }
        return true;
    }
    public static boolean isPrime(double num) {
        return isPrime((int)num);
    }
}
