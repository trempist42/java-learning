public class Hello {
    public static void main(String[] args) {
        System.out.println("1+2="+sum(1,2));
        System.out.println("1.5+2.5="+sum(1.5d,2.5d));
    }
    public static int sum(int x, int y) {
        System.out.println("Adding 2 int entities");
        return x + y;
    }
    public static double sum(double x, double y) {
        System.out.println("Adding 2 int entities");
        return x + y;
    }
}
